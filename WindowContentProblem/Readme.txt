﻿This repo demonstrates problem with moving content from one window to another. 
This causes behavior where first window appears to be blocked and content of the control is still visible (like it was not actually removed) or it sometimes throws error that control still has parent. 
If you delay update of content in second window it works fine. So this is probably problem with synchronization. 
I tried different workarounds like for example attaching to LayoutUpdated event of parent in first window to make sure removing from parent was processed or setting up content in second window on Activate but it didn't help. 
Only thing that works is sufficient arbitrary delaying before setting up content of the second window but this is obviously not an option.