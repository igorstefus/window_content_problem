﻿using System;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Presenters;
using Avalonia.Data;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using WindowContentProblem.ViewModels;

namespace WindowContentProblem.Views
{
    public class MainWindow : Window
    {
        private readonly Panel _container;
        private readonly ContentControl _content;

        public MainWindow()
        {
            InitializeComponent();

            _container = this.FindControl<Panel>("container");
            _content = this.FindControl<ContentControl>("content");
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            if (_container.Children.Contains(_content))
            {
                Window window = new Window
                {
                    Owner = this,
                    Width = 400, 
                    Height = 300, 
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };

                _container.Children.Remove(_content);

                window.Content = _content;

                window.Show();

                //If we delay content assignment then there is no problem with content moving. 

                //Dispatcher.UIThread.Post(async () =>
                //{
                //    await Task.Delay(500);
                //    window.Content = _content;
                //});

                //window.Show();
            }
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
